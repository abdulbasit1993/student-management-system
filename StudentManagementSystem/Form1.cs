﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace StudentManagementSystem
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void addStudentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            student_info st = new student_info();
            st.Show();
        }

        private void logoutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void aboutStudentManagementSystemToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("This is a Software for Managing Students Data in a Database. Made by Abdul Basit with Group Members: Saad Salim and Shahbaz Hussain");
        }
    }
}
